#!/usr/bin/env bash

sudo -u postgres psql << EOF
    CREATE USER "tester" WITH PASSWORD 'tester';
    CREATE DATABASE testing;
    GRANT ALL PRIVILEGES ON DATABASE testing to "tester";
    ALTER ROLE "tester" superuser;
EOF

sudo -u postgres psql -d testing < _data/dump.sql