<?php
use Nette\Caching\Storages\DevNullStorage;
use Nette\Loaders\RobotLoader;

$loader = new RobotLoader();
$loader->setCacheStorage(new DevNullStorage);
$loader->addDirectory(__DIR__ . '/_data/project/app');
$loader->register();