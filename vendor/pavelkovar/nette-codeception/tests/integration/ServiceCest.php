<?php

use Codeception\Lib\Connector\Nette\Http\HttpResponse;
use Nette\Http\IResponse;
use Test\App\Service\MyService;

class ServiceCest
{
    public function grabService(IntegrationTester $I)
    {
        $I->wantTo('grab service from DI container');

        $I->assertInstanceOf(HttpResponse::class, $I->grabService(IResponse::class));
        $I->assertInstanceOf(HttpResponse::class, $I->grabServiceByName('http.response'));
    }

    public function addService(IntegrationTester $I)
    {
        $I->wantTo('register service to DI container');

        $I->expectException(PHPUnit_Framework_AssertionFailedError::class, function () use ($I) {
            $I->grabServiceByName('my.service');
        });

        $I->addService('my.service', new MyService());
        $I->assertInstanceOf(MyService::class, $I->grabServiceByName('my.service'));

        $I->amOnPage('/homepage/page');
        $I->assertInstanceOf(MyService::class, $I->grabServiceByName('my.service'));
    }

    public function grabFromExtension(IntegrationTester $I)
    {
        $I->wantToTest('that service from extension is registered');

        $I->assertInstanceOf(MyService::class, $I->grabServiceByName('custom.service'));
    }
}
