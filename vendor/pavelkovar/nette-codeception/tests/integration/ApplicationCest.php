<?php

class ApplicationCest
{
    public function openPage(IntegrationTester $I)
    {
        $I->wantTo('open existing pages');
        $I->amOnPage('/');
        $I->seeResponseCodeIs(200);
        $I->see('Main page', 'h1');

        $I->amOnPage('/homepage/page');
        $I->seeResponseCodeIs(200);
        $I->see('Second page', 'h2');
    }

    public function openMissingPage(IntegrationTester $I)
    {
        $I->wantTo('open missing pages');

        $I->amOnPage('/homepage/missing');
        $I->seeResponseCodeIs(404);

        $I->amOnPage('/missing/default');
        $I->seeResponseCodeIs(404);
    }

    public function testRoute(IntegrationTester $I)
    {
        $I->wantTo('open page using route and see that i am on right page');

        $routeArgs = ['variable' => 'hello world!', 'example' => 'example value'];

        $I->amOnRoute('Homepage:variable', $routeArgs);
        $I->seeResponseCodeIs(200);
        $I->seeCurrentRouteEquals('Homepage:variable', $routeArgs);
        $I->dontSeeCurrentRouteEquals('Homepage:variable', ['variable' => 'different route']);
        $I->seeInCurrentRoute('Homepage:variable', ['variable' => 'hello world!']);
        $I->dontSeeInCurrentRoute('Homepage:variable', ['example' => 'example value']);
        $I->see('hello world!', 'p');

        $I->amOnRoute('Homepage:default');
        $I->seeResponseCodeIs(200);
        $I->seeInCurrentRoute('Homepage:default');
        $I->dontSeeCurrentRouteEquals('Homepage:variable', $routeArgs);

        $I->amOnRoute('Homepage:missing');
        $I->seeResponseCodeIs(404);
        $I->seeCurrentRouteEquals('Homepage:missing');
        $I->dontSeeCurrentRouteEquals('Homepage:other');
    }

    public function testForms(IntegrationTester $I)
    {
        $I->wantTo('submit form and see if any error occurs');

        $I->amOnPage('/form/default');
        $I->seeResponseCodeIs(200);
        $I->dontSeeFormHasErrors('exampleForm');

        $I->submitForm('#frm-exampleForm', [
            'width' => 1,
            'height' => 2
        ]);
        $I->dontSeeFormHasErrors('exampleForm');

        $I->submitForm('#frm-exampleForm', [
            'width' => 'hello',
            'height' => 2
        ]);
        $I->seeFormHasErrors('exampleForm');
    }
}
