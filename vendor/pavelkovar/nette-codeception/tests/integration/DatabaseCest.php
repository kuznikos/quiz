<?php

use Nette\Database\Connection;
use Nette\Database\Context;

class DatabaseCest
{
    public function testDatabase(IntegrationTester $I)
    {
        $I->wantToTest('database handler');

        $I->seeInDatabase('user');
        $I->seeInDatabase('user', ['firstname' => 'Eva']);
        $I->seeInDatabase('user', ['firstname' => 'Jan', 'lastname' => 'Novák']);

        $I->dontSeeInDatabase('user', ['firstname' => 'Pepa']);

        $column = $I->grabFromDatabase('user', 'firstname', ['firstname' => 'Eva']);
        $I->assertEquals('Eva', $column);

        $column = $I->grabFromDatabase('user', 'username', ['firstname' => 'none']);
        $I->assertEquals(false, $column);
    }

    public function testDatabaseShare(IntegrationTester $I)
    {
        $I->wantToTest('database sharing between test requests');

        $I->amOnPage('/');
        $prev = $I->grabService(Connection::class);

        $I->amOnPage('/');
        $curr = $I->grabService(Connection::class);

        $I->assertSame($prev, $curr);
    }

    public function modifyDatabase(IntegrationTester $I)
    {
        $I->wantTo('modify database');

        /** @var Context $db */
        $db = $I->grabService(Context::class);
        $db->table('user')->insert([
            'username' => 'dbtester',
            'firstname' => 'database',
            'lastname' => 'tester'
        ]);
        $I->seeInDatabase('user', ['username' => 'dbtester']);
    }

    public function testCleanup(IntegrationTester $I)
    {
        $I->wantToTest('transaction cleanup');
        $I->dontSeeInDatabase('user', ['username' => 'dbtester']);
    }
}