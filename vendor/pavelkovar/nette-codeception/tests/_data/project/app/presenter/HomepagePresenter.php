<?php
namespace Test\App\Presenter;

use Nette\Application\UI\Presenter;

class HomepagePresenter extends Presenter
{
    public function actionVariable($variable)
    {

    }

    public function renderVariable($variable)
    {
        $this->template->variable = $variable;
    }
}