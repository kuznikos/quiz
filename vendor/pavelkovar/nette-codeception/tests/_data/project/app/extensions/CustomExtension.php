<?php
namespace Test\App\Extensions;

use Nette\DI\CompilerExtension;
use Test\App\Service\MyService;

class CustomExtension extends CompilerExtension
{
    public function beforeCompile()
    {
        $builder = $this->getContainerBuilder();

        $builder->addDefinition($this->prefix('service'))
            ->setClass(MyService::class);
    }
}