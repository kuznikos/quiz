<?php
namespace Codeception\Lib\Connector;

use Codeception\Lib\Connector\Nette\Environment;
use Codeception\Lib\Connector\Nette\Http\HttpResponse;
use Exception;
use Nette\Application\Application;
use Nette\Caching\Storages\IJournal;
use Nette\Caching\Storages\SQLiteJournal;
use Nette\Configurator;
use Nette\Database\Connection;
use Nette\DI\Container;
use Nette\DI\MissingServiceException;
use Nette\Http\IResponse;
use Nette\Http\Session;
use ReflectionProperty;
use Symfony\Component\BrowserKit\Client;
use Symfony\Component\BrowserKit\Request;
use Symfony\Component\BrowserKit\Response;

class Nette extends Client
{
    /** Name of nette database service. */
    const DB_SERVICE = 'database.default.connection';
    /** Name of cache journal service. */
    const JOURNAL_SERVICE = 'cache.journal';
    /** Name of http request in container. */
    const HTTP_REQUEST = 'http.request';

    /** @var Container */
    private $container;
    /** @var Environment */
    private $environment;
    /** @var array */
    private $services = [];

    /**
     * Construct nette connector.
     *
     * @param Environment $env
     */
    public function __construct(Environment $env)
    {
        parent::__construct();
        $this->environment = $env;

        $this->followRedirects(true);
        $this->initialize();
    }

    /**
     * Initialize connector.
     */
    private function initialize()
    {
        // Create temp folder
        $this->environment->createTempDirectory();

        // Set default server params
        $_SERVER = $this->environment->getServerParams();

        // Create initial container
        $this->bootApplication();

        // Run database in transaction
        if ($this->environment->isCleanupEnabled()) {
            $db = $this->getDatabase(true);
            if ($db) {
                $db->beginTransaction();
            }
        }
    }

    /**
     * Initialize configurator.
     */
    private function bootApplication()
    {
        // Create configurator
        $configurator = $this->environment->initializeConfigurator();

        // Reuse shared resources
        $this->reuseResources($configurator);

        // Build container
        $this->container = $configurator->createContainer();
    }

    /**
     * Process HTTP request.
     *
     * @param Request $request request
     * @return Response response
     * @throws Exception
     */
    protected function doRequest($request)
    {
        // Set super global variables
        $method = strtoupper($request->getMethod());

        $_SERVER = array_merge($this->environment->getServerParams(), $request->getServer());
        $_SERVER['REQUEST_URI'] = $this->parseRequestUri($request->getUri());
        $_SERVER['REQUEST_METHOD'] = $method;

        if (in_array($method, ['GET', 'HEAD'])) {
            $_GET = $request->getParameters();
            $_POST = [];
        } else {
            $_POST = $request->getParameters();
            $_GET = [];
        }

        $_COOKIE = $request->getCookies();
        $_FILES = $request->getFiles();

        // Recreate container if any request is created before
        if ($this->hasCreatedService(self::HTTP_REQUEST)) {
            $this->bootApplication();
        }

        // Run application
        try {
            ob_start();
            $this->getServiceByType(Application::class)->run();
            $content = ob_get_clean();
        } catch (Exception $e) {
            ob_end_clean();
            throw $e;
        }

        /** @var HttpResponse $response */
        $response = $this->getServiceByType(IResponse::class);
        return $response->toBrowserResponse($content);
    }

    /**
     * Retrieve an instance of class from the DI container by class name.
     *
     * @param string $class name of class to retrieve
     * @param bool $throw throw exception if service is not found
     * @return object|null returns null when container is not initialized or when $throw parameter is set to false and service is not found, otherwise found service
     * @throws MissingServiceException when $throw parameter is set to true and service is not found in container
     */
    public function getServiceByType($class, $throw = false)
    {
        if (!$this->container) {
            return null;
        }

        try {
            return $this->container->getByType($class);
        } catch (MissingServiceException $e) {
            if ($throw) {
                throw $e;
            }
            return null;
        }
    }

    /**
     * Retrieve an instance of class from the DI container by service name.
     *
     * @param string $serviceName name of service to retrieve
     * @return object|null null if container is not initialized otherwise found service
     * @throws MissingServiceException when service is not found in container
     */
    public function getServiceByName($serviceName)
    {
        return ($this->container) ? $this->container->getService($serviceName) : null;
    }

    /**
     * Adds the service to DI container.
     *
     * @param string $name
     * @param object $object
     */
    public function addService($name, $object)
    {
        $this->services[$name] = $object;

        if ($this->container) {
            $this->container->addService($name, $object);
        }
    }

    /**
     * Reuse resources (like database connection) from previous container in new one.
     *
     * @param Configurator $configurator
     */
    private function reuseResources(Configurator $configurator)
    {
        // Database connection
        $db = $this->getDatabase();
        if ($db) {
            $this->services[self::DB_SERVICE] = $db;
        }

        // Cache journal
        $journal = $this->getJournal();
        if ($journal) {
            $this->services[self::JOURNAL_SERVICE] = $journal;
        }
        $configurator->addServices($this->services);
    }

    /**
     * Close resources to avoid keeping them active between tests.
     */
    public function closeResources()
    {
        // Close session
        /** @var Session $session */
        $session = $this->getServiceByType(Session::class);
        if ($session && $session->isStarted()) {
            $session->destroy();
        }

        // Close database
        $database = $this->getDatabase();
        if ($database) {
            if ($this->environment->isCleanupEnabled()) {
                $database->rollBack();
            }
            $database->disconnect();
        }

        // Close journal
        $journal = $this->getJournal();
        if ($journal) {
            $property = new ReflectionProperty(SQLiteJournal::class, 'pdo');
            $property->setAccessible(true);
            $property->setValue($journal, null);
        }

        // Clean temp folder
        $this->environment->deleteTempDirectory();
    }

    /**
     * Parse path from request URL.
     *
     * @param string $url
     * @return string
     */
    public function parseRequestUri($url)
    {
        preg_match(',http[s]?://[a-zA-Z]*(/.*),i', $url, $matches);
        return $matches[1];
    }

    /**
     * Gets database connection from container.
     *
     * @param bool $create create connection if is not created yet
     * @return Connection|null instance of Context if class exists and instance is in container, otherwise null
     */
    private function getDatabase($create = false)
    {
        if (!$this->hasDatabaseModule()) {
            return null;
        }
        return ($create || $this->hasCreatedService(self::DB_SERVICE)) ? $this->getServiceByType(Connection::class) : null;
    }

    /**
     * Gets caching journal from container.
     *
     * @return SQLiteJournal|null instance of SQLLiteJournal if class exists and instance is in container, otherwise null
     */
    private function getJournal()
    {
        if (!$this->hasCreatedService(self::JOURNAL_SERVICE)) {
            return null;
        }

        $journal = $this->getServiceByType(IJournal::class);
        return ($journal && $journal instanceof SQLiteJournal) ? $journal : null;
    }

    /**
     * Check if service with given name is already created in DI container.
     *
     * @param string $serviceName
     * @return bool if created true otherwise false
     */
    private function hasCreatedService($serviceName)
    {
        return !is_null($this->container) && $this->container->isCreated($serviceName);
    }

    /**
     * Check if nette database package is registered in classpath.
     *
     * @return bool if available true otherwise false
     */
    public function hasDatabaseModule()
    {
        return class_exists('\Nette\Database\Connection');
    }
}