<?php
namespace Codeception\Lib\Connector\Nette;

use Nette\Configurator;
use Nette\DI\Compiler;
use Nette\Utils\FileSystem;

class Environment
{
    /** @var string */
    private $tempDir;
    /** @var string|null */
    private $logDir;
    /** @var array */
    private $parameters;
    /** @var array */
    private $configFiles;
    /** @var array */
    private $config;
    /** @var bool */
    private $cleanup;
    /** @var string */
    private $timeZone;
    /** @var array */
    private $serverParams = [
        'SERVER_NAME' => 'localhost',
        'REMOTE_ADDR' => '127.0.0.1'
    ];
    /** @var array */
    private $extensions;

    /**
     * Construct environment.
     *
     * @param array $config
     */
    public function __construct(array $config)
    {
        $appDir = $this->getPath($config, 'appDir');
        $wwwDir = $this->getPath($config, 'wwwDir');

        $this->tempDir = $this->getPath($config, 'tempDir');
        $this->logDir = $this->getPath($config, 'logDir');
        $this->parameters = array_merge($config['parameters'], ['appDir' => $appDir, 'wwwDir' => $wwwDir]);
        $this->cleanup = $config['cleanup'];
        $this->timeZone = $config['timeZone'];
        $this->configFiles = [];
        $this->config = $config['config'];
        $this->extensions = array_merge($config['extensions'], ['codeception' => CodeceptionExtension::class]);

        foreach ($config['configFiles'] as $file) {
            $this->configFiles[] = $appDir . DIRECTORY_SEPARATOR . $file;
        }

        if (!is_null($this->logDir)) {
            FileSystem::createDir($this->logDir);
        }
    }

    /**
     * Create temp folder.
     */
    public function createTempDirectory()
    {
        FileSystem::createDir($this->tempDir);
    }

    /**
     * Delete temp folder from filesystem.
     */
    public function deleteTempDirectory()
    {
        FileSystem::delete($this->tempDir);
    }

    /**
     * Initialize Nette bootstrap class.
     *
     * @return Configurator
     */
    public function initializeConfigurator()
    {
        $configurator = new Configurator();
        $configurator->addParameters($this->parameters);
        $configurator->setTempDirectory($this->tempDir);
        $configurator->setDebugMode(false);
        if (!is_null($this->timeZone)) {
            $configurator->setTimeZone($this->timeZone);
        }
        if (!is_null($this->logDir)) {
            $configurator->enableDebugger($this->logDir);
        }
        foreach ($this->configFiles as $configFile) {
            $configurator->addConfig($configFile);
        }
        $configurator->onCompile[] = function (Configurator $configurator, Compiler $compiler) {
            foreach ($this->extensions as $name => $class) {
                $compiler->addExtension($name, new $class);
            }
            // Override configuration
            $compiler->addConfig($this->config);
        };
        return $configurator;
    }

    /**
     * Check if tests should run in database transaction.
     *
     * @return bool
     */
    public function isCleanupEnabled()
    {
        return $this->cleanup;
    }

    /**
     * Gets default server parameters.
     *
     * @return array
     */
    public function getServerParams()
    {
        return $this->serverParams;
    }

    /**
     * Returns absolute path from Codeception root to requested directory.
     *
     * @param array $config
     * @param string $directory
     * @return string
     */
    private function getPath($config, $directory)
    {
        return isset($config[$directory]) ? codecept_root_dir($config[$directory]) : null;
    }
}