<?php
namespace Codeception\Lib\Connector\Nette;

use Codeception\Lib\Connector\Nette\Cache\FileStorage;
use Codeception\Lib\Connector\Nette\Http\HttpResponse;
use Nette\Caching\IStorage;
use Nette\Caching\Storages\FileStorage as NetteFileStorage;
use Nette\DI\CompilerExtension;
use Nette\Http\IResponse;

class CodeceptionExtension extends CompilerExtension
{
    public function loadConfiguration()
    {
        $builder = $this->getContainerBuilder();

        $response = $builder->getByType(IResponse::class) ?: 'httpResponse';
        if ($builder->hasDefinition($response)) {
            $builder->getDefinition($response)
                ->setClass(IResponse::class)
                ->setFactory(HttpResponse::class);
        }

        $storage = $builder->getByType(IStorage::class) ?: 'cacheStorage';
        if ($builder->hasDefinition($storage)) {
            $def = $builder->getDefinition($storage);
            $factory = $def->getFactory();

            if ($factory->entity == NetteFileStorage::class) {
                $def->setClass(IStorage::class)
                    ->setFactory(FileStorage::class, $factory->arguments);
            }
        }
    }
}