<?php
namespace Codeception\Lib\Connector\Nette\Http;

use Nette\Http\Helpers;
use Nette\Http\IResponse;
use Nette\Utils\DateTime;
use Symfony\Component\BrowserKit\Response;

/**
 * Dummy implementation of \Nette\Http\Response.
 * @package Codeception\Lib\Connector\Nette
 */
class HttpResponse implements IResponse
{
    /** @var int HTTP code */
    private $code = self::S200_OK;
    /** @var array */
    private $headers = [];

    /**
     * Sets HTTP response code.
     * @param int $code
     * @return self
     */
    public function setCode($code)
    {
        $this->code = $code;
        return $this;
    }

    /**
     * Returns HTTP response code.
     * @return int
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Sends a HTTP header and replaces a previous one.
     * @param string $name
     * @param string $value
     * @return self
     */
    public function setHeader($name, $value)
    {
        $this->headers[$name] = $value;
        return $this;
    }

    /**
     * Adds HTTP header.
     * @param string $name
     * @param string $value
     * @return self
     */
    public function addHeader($name, $value)
    {
        $this->headers[$name] = $value;
        return $this;
    }

    /**
     * Sends a Content-type HTTP header.
     * @param string $type mime-type
     * @param string $charset
     * @return self
     */
    public function setContentType($type, $charset = NULL)
    {
        $this->setHeader('Content-Type', $type . ($charset ? '; charset=' . $charset : ''));
        return $this;
    }

    /**
     * Redirects to a new URL.
     * @param string $url
     * @param int $code
     */
    public function redirect($url, $code = self::S302_FOUND)
    {
        $this->setCode($code);
        $this->setHeader('Location', $url);
    }

    /**
     * Sets the number of seconds before a page cached on a browser expires.
     * @param string|int|\DateTimeInterface $time , value 0 means "until the browser is closed"
     * @return self
     */
    public function setExpiration($time)
    {
        $this->setHeader('Pragma', NULL);
        if (!$time) { // no cache
            $this->setHeader('Cache-Control', 's-maxage=0, max-age=0, must-revalidate');
            $this->setHeader('Expires', 'Mon, 23 Jan 1978 10:00:00 GMT');
            return $this;
        }

        $time = DateTime::from($time);
        $this->setHeader('Cache-Control', 'max-age=' . ($time->format('U') - time()));
        $this->setHeader('Expires', Helpers::formatDate($time));
        return $this;
    }

    /**
     * Checks if headers have been sent.
     * @return bool
     */
    public function isSent()
    {
        return false;
    }

    /**
     * Returns value of an HTTP header.
     * @param string $header
     * @param mixed $default
     * @return mixed
     */
    public function getHeader($header, $default = NULL)
    {
        return array_key_exists($header, $this->headers) ? $this->headers[$header] : $default;
    }

    /**
     * Returns a list of headers to sent.
     * @return array (name => value)
     */
    public function getHeaders()
    {
        return $this->headers;
    }

    /**
     * Sends a cookie.
     * @param string $name of the cookie
     * @param string $value
     * @param mixed $expiration as unix timestamp or number of seconds; Value 0 means "until the browser is closed"
     * @param string $path
     * @param string $domain
     * @param bool $secure
     * @param bool $httpOnly
     * @return self
     */
    public function setCookie($name, $value, $expiration, $path = NULL, $domain = NULL, $secure = NULL, $httpOnly = NULL)
    {
        return $this;
    }

    /**
     * Deletes a cookie.
     * @param string $name of the cookie.
     * @param string $path
     * @param string $domain
     * @param bool $secure
     * @return self
     */
    public function deleteCookie($name, $path = NULL, $domain = NULL, $secure = NULL)
    {
        return $this;
    }

    /**
     * Convert Nette response to BrowserKit response.
     * @param string $content
     * @return Response
     */
    public function toBrowserResponse($content)
    {
        return new Response($content, $this->code, $this->headers);
    }
}