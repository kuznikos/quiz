<?php
namespace Codeception\Lib\Connector\Nette\Cache;

use Nette\Caching\Storages\FileStorage as BaseStorage;

class FileStorage extends BaseStorage
{
    /** @inheritdoc */
    public function lock($key)
    {
        $cacheDir = dirname(dirname($this->getCacheFile($key)));
        if (is_dir($cacheDir)) {
            parent::lock($key);
        }
    }
}