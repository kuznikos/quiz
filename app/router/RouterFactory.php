<?php
namespace App;

use Nette\Application\IRouter;
use Nette\Application\Routers\Route;
use Nette\Application\Routers\RouteList;

class RouterFactory
{
    /**
     * @return IRouter
     */
    public static function createRouter()
    {
        $router = new RouteList;
        $router[] = new Route('sw/<presenter>/<action>/<id>', array(
            'module' => 'SW',
            'presenter' => 'Homepage',
            'action' => 'default',
            'id' => NULL,
        ));
        $router[] = new Route('test/<presenter>/<action>/<id>', array(
            'module' => 'Test',
            'presenter' => 'Homepage',
            'action' => 'default',
            'id' => NULL,
        ));
        $router[] = new Route('admin/<presenter>/<action>/<id>', array(
            'module' => 'Admin',
            'presenter' => 'Homepage',
            'action' => 'default',
            'id' => NULL,
        ));
        $router[] = new Route('students/<presenter>/<action>/<id>', array(
            'module' => 'Students',
            'presenter' => 'Homepage',
            'action' => 'default',
            'id' => NULL,
        ));
        $router[] = new Route('database/<action>/<id>', array(
            'module' => 'Database',
            'presenter' => 'Homepage',
            'action' => 'default',
            'id' => NULL,
        ));
        $router[] = new Route('users/<presenter>/<action>/<id>', array(
            'module' => 'Users',
            'presenter' => 'Homepage',
            'action' => 'default',
            'id' => NULL,
        ));
        $router[] = new Route('<presenter>/<action>[/<id>]', 'Homepage:default');
        return $router;
    }
}
