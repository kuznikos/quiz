<?php
namespace App\Model\Post;

use Nette\Database\Context;
use Nette\Database\Table\IRow;

class PostRepository
{
    const TABLE_NAME = 'posts';

    /** @var Context */
    private $db;

    /**
     * PostRepository constructor.
     * @param Context $db
     */
    public function __construct(Context $db)
    {
        $this->db = $db;
    }

    /**
     * Store instance of {@link Post} to database.
     * @param Post $post
     */
    public function store(Post $post)
    {
        $values = [
            'name' => $post->getName(),
            'content' => $post->getContent(),
            'author' => $post->getAuthor()
        ];

        if ($post->getId() === null) {
            $result = $this->db->table(self::TABLE_NAME)->insert($values);
            $post->setId($result['post_id']);
        } else {
            $this->db->table(self::TABLE_NAME)
                ->where('post_id', $post->getId())
                ->update($values);
        }
    }

    /**
     * Delete post from database.
     * @param Post $post
     */
    public function remove(Post $post)
    {
        $this->db->table(self::TABLE_NAME)->where('post_id', $post->getId())->delete();
    }

    /**
     * Fetch all posts from database.
     * @return Post[]
     */
    public function fetchAll()
    {
        $query = $this->db->table(self::TABLE_NAME);
        $result = [];
        foreach ($query as $row) {
            $result[] = $this->rowToPost($row);
        }
        return $result;
    }

    /**
     * Fetch post from database by ID.
     * @param int $postId
     * @return Post|null
     */
    public function fetchById($postId)
    {
        $query = $this->db->table(self::TABLE_NAME)->where('post_id', $postId)->fetch();
        return ($query === false) ? null : $this->rowToPost($query);
    }

    /**
     * Create {@link Post} from result row.
     * @param IRow $row
     * @return Post
     */
    private function rowToPost(IRow $row)
    {
        $post = new Post();
        $post->setId($row['post_id']);
        $post->setName($row['name']);
        $post->setContent($row['content']);
        $post->setAuthor($row['author']);
        return $post;
    }
}