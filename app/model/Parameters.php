<?php
namespace App\Model;

/**
 * Class Parameters
 * @package App\Model
 */
class Parameters
{

    /**
     * @var array
     */
    private $parameters;

    /**
     * Parameters constructor.
     * @param array $parameters
     */
    public function __construct(array $parameters)
    {
        $this->parameters = $parameters;
    }

    /**
     * @param $name
     * @return mixed|string
     */
    public function getParameter($name)
    {
        if (isset($this->parameters[$name])) {
            return $this->parameters[$name];
        } else {
            return '';
        }
    }


}
