<?php
namespace App\Model;

use Nette\Database\Context;
use Nette\Database\Table\Selection;

/**
 * Class Users
 * @package App\Model
 */
class Users
{
    /** @var Context */
    private $database;

    /**
     * Users constructor.
     * @param Context $db
     */
    public function __construct(Context $db)
    {
        $this->database = $db;
    }

    /**
     * Get information about user by username.
     * @param string $username
     * @return array|null
     */
    public function getUser($username)
    {
        $result = $this->database->table('user')->where('username', $username)->fetch();
        return $result ? $result->toArray() : null;
    }
}