<?php
namespace App\Forms;

use App\Forms\Rendering\AdminLteFormRenderer;
use Kdyby\Translation\Translator;
use Nette\Application\UI\Form;

class FormFactory
{
    /** @var Translator */
    private $translator;

    /**
     * FormFactory constructor.
     * @param Translator $translator
     */
    public function __construct(Translator $translator)
    {
        $this->translator = $translator;
    }

    /**
     * @return Form
     */
    public function create()
    {
        $form = new Form;
        $form->setTranslator($this->translator);
        $form->setRenderer(new AdminLteFormRenderer());
        return $form;
    }
}
