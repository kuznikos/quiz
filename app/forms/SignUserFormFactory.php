<?php
namespace App\Forms;

use App\Security\Authenticator\CredentialsAuthenticator;
use Nette\Application\UI\Form;
use Nette\Security\AuthenticationException;

class SignUserFormFactory
{
    /** @var FormFactory */
    private $factory;
    /** @var CredentialsAuthenticator */
    private $authenticator;

    public function __construct(FormFactory $factory, CredentialsAuthenticator $authenticator)
    {
        $this->factory = $factory;
        $this->authenticator = $authenticator;
    }

    /**
     * @return Form
     */
    public function create()
    {
        $form = $this->factory->create();
        $form->addText('username', 'base.forms.username')
            ->setRequired('base.forms.usernameRequired');
        $form->addSubmit('send', 'base.forms.signIn');
        $form->onSuccess[] = [$this, 'formSucceeded'];
        return $form;
    }

    public function formSucceeded(Form $form, $values)
    {
        try {
            $this->authenticator->login([$values->username]);
        } catch (AuthenticationException $e) {
            $form->addError($form->getTranslator()->translate($e->getMessage()));
        }
    }
}
