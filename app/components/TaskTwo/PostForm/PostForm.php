<?php
namespace App\Components\TaskTwo\PostForm;

use App\Forms\FormFactory;
use App\Model\Post\Post;
use App\Model\Post\PostRepository;
use Nette\Application\UI\Control;
use Nette\Forms\Controls\SubmitButton;
use Nette\Security\User;

/**
 * Class PostForm
 * @package App\Components\TaskTwo\PostForm
 *
 * @method onSend()
 * @method onReturn()
 */
class PostForm extends Control
{
    /** @var FormFactory */
    private $formFactory;
    /** @var User */
    private $user;
    /** @var PostRepository */
    private $postRepository;

    /** @var Post|null */
    private $post;
    /** @var bool */
    private $isNew;

    /** @var callable[] */
    public $onSend;
    /** @var callable[] */
    public $onReturn;

    /**
     * PostFormFactory constructor.
     * @param Post|null $post
     * @param FormFactory $formFactory
     * @param User $user
     * @param PostRepository $postRepository
     */
    public function __construct($post, FormFactory $formFactory, User $user, PostRepository $postRepository)
    {
        parent::__construct();
        $this->post = $post;
        $this->formFactory = $formFactory;
        $this->user = $user;
        $this->postRepository = $postRepository;

        $this->isNew = $this->post === null;
    }

    /**
     * @return \Nette\Application\UI\Form
     */
    public function createComponentForm()
    {
        $default = ($this->isNew) ? $this->createDefaultValues() : $this->post;

        $form = $this->formFactory->create();
        $form->addText('name', 'Název')
            ->addRule($form::MAX_LENGTH, 'Your note is way too long', 40)
            ->setRequired()
            ->setDefaultValue($default->getName());

        $form->addTextArea('content', 'Obsah')
            ->setRequired(false)
            ->addRule($form::MIN_LENGTH, 'Your note is way too short', 5)
            ->addRule($form::MAX_LENGTH, 'Your note is way too long', 700)
            ->setDefaultValue($default->getContent());

        $form->addText('author', 'Autor')
            ->setRequired()
            ->addRule($form::MAX_LENGTH, 'Your note is way too long', 30)
            ->setDefaultValue($default->getAuthor());

        $form->addSubmit('send', ($this->isNew) ? 'Vytvořit příspěvek' : 'Upravit příspěvek')
            ->onClick[] = [$this, 'onSendBtnClick'];

        $form->addSubmit('back', 'Zpět na seznam')
            ->setValidationScope(false)
            ->onClick[] = [$this, 'onReturn'];
        return $form;
    }

    /**
     * Return form default values.
     * @return Post
     */
    private function createDefaultValues()
    {
        $post = new Post();
        $post->setAuthor($this->user->getIdentity()->username);
        return $post;
    }
    public function onSendBtnClick(SubmitButton $btn)
    {
        // Fetch form values
        $values = $btn->form->getValues();

        // Create and persist post
        $post = new Post();
        if (!$this->isNew) {
            $post->setId($this->post->getId());
        }
        $post->setName($values['name']);
        $post->setContent($values['content']);
        $post->setAuthor($values['author']);
        $this->postRepository->store($post);

        // Call listeners
        $this->onSend();
    }

    public function render()
    {
        $this['form']->render();
    }
}