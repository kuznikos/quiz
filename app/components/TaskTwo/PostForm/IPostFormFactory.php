<?php
namespace App\Components\TaskTwo\PostForm;

use App\Model\Post\Post;

interface IPostFormFactory
{
    /**
     * @param Post $post
     * @return PostForm
     */
    public function create($post);
}