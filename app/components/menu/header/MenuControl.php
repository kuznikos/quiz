<?php
namespace App\Components\Menu\Header;

use DK;

class MenuControl extends DK\Menu\UI\Control {}

interface IMenuControlFactory
{
    /**
     * @return MenuControl
     */
    public function create();

}
