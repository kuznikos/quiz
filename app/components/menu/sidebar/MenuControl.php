<?php
namespace App\Components\Menu\Sidebar;

use DK;

class MenuControl extends DK\Menu\UI\Control {}

interface IMenuControlFactory
{
    /**
     * @return MenuControl
     */
    public function create();

}
