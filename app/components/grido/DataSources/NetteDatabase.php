<?php
namespace App\Components\Grido\DataSources;

use Grido\Components\Filters\Condition;
use Grido\Exception;
use Latte\Runtime\Filters;

/**
 * Class NetteDatabase
 * @package App\Components\Grido\DataSources
 */
class NetteDatabase extends \Grido\DataSources\NetteDatabase
{
    /** @inheritdoc */
    public function suggest($column, array $conditions, $limit)
    {
        // Create new selection
        $selection = clone $this->selection;
        $selection->limit($limit);

        // Add new where condition
        foreach ($conditions as $condition) {
            $this->makeWhere($condition, $selection);
        }

        // Select $column only
        $items = [];
        foreach ($selection as $row) {
            if (is_string($column)) {
                $value = (string)$row[$column];
            } elseif (is_callable($column)) {
                $value = (string)$column($row);
            } else {
                $type = gettype($column);
                throw new Exception("Column of suggestion must be string or callback, $type given.");
            }
            $items[] = Filters::escapeHtml($value);
        }

        // Get sorted unique values
        $items = array_unique($items);
        sort($items);
        return $items;
    }

    /** @inheritdoc */
    protected function makeWhere(Condition $condition, \Nette\Database\Table\Selection $selection = NULL)
    {
        // Set selection
        $selection = ($selection === NULL) ? $this->selection : $selection;

        // Append condition to selection
        if ($condition->callback) {
            call_user_func_array($condition->callback, [$condition->value, $selection]);
        } else {
            // If condition starts with LIKE, make it case insensitive
            $cond = $condition->getCondition();
            $value = $condition->getValue();
            if (isset($cond[0]) && isset($value[0])) {
                $cond = $cond[0];
                // Check if cond is string
                if (is_string($cond) && strpos($cond, "LIKE") === 0) {
                    $value = $value[0];
                    // Check if value is date
                    if (preg_match('/^[0-9]{4}-[0-9]{2}-[0-9]{2}%$/', $value)) {
                        $column = $condition->getColumn()[0] . "::text";
                        $condition->setColumn($column);
                    } // Check if value is strings
                    else if (!is_numeric($value)) {
                        $condition->setCondition('ILIKE ?');
                    }
                }
            }
            // Add condition to query
            call_user_func_array(array($selection, 'where'), $condition->__toArray());
        }
    }
}
