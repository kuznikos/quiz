<?php
namespace App\Components\Grido;

use App\Components\Grido\DataSources\NetteDatabase;
use App\Service\DateService;
use Grido\Components\Columns\Column;
use Grido\Components\Filters\Filter;
use Grido\Grid as BaseGrid;
use Kdyby\Translation\Translator;
use Nette;
use Nette\Reflection\ClassType;
use Nette\Database\Table\Selection;

/**
 * Class Grid
 * @package App\Components\Grido
 */
class Grid extends BaseGrid
{
    /** @var Translator */
    private $netteTranslator;
    /** @var DateService */
    private $dateService;
    /** @var array */
    private $classAnnotations = ['Primary', 'RenderRow', 'Action', 'DefaultSort'];
    /** @var array */
    private $propertyAnnotations = ['Sortable', 'Render', 'Filter'];

    /**
     * Grid constructor.
     * @param Translator|null $translator
     * @param DateService $dateService
     */
    public function __construct(Translator $translator = null, DateService $dateService = null)
    {
        parent::__construct();
        if ($translator != null) {
            $this->setCustomTranslator($translator);
        }
        if ($dateService != null) {
            $this->dateService = $dateService;
        }
        $this->setFilterRenderType(Filter::RENDER_INNER);
        $this->setTemplateFile(__DIR__ . '/template/default.latte');
        $this->rememberState = true;
    }

    /**
     * Sets model and create grid from entity annotations
     * @param mixed $model
     * @param object|string $entity annotated class, object instance or <b>class name</b> with namespace are accepted
     * @throws \Exception when $entity has invalid type
     */
    public function setAnnotatedModel($model, $entity)
    {
        // Set model
        $this->setModel($model);

        // Get entity reflection
        if (is_string($entity)) {
            $reflection = new ClassType($entity);
        } else if (is_object($entity)) {
            $reflection = new ClassType(get_class($entity));
        } else {
            throw new \Exception('Entity should be object instance or class name with namespace.');
        }

        // Parse class annotations
        foreach ($this->classAnnotations as $name) {
            if ($reflection->hasAnnotation($name)) {
                $method = "parseAnnotation$name";
                $this->$method($reflection->getAnnotation($name));
            }
        }
        
        // Parse property annotations
        foreach ($reflection->getProperties() as $property) {
            if ($property->hasAnnotation('Column')) {
                $this->parsePropertyAnnotation($property);
            }
        }
    }

    /**
     * Sets custom translator instance.
     * @param Nette\Localization\ITranslator $translator
     */
    public function setCustomTranslator(Nette\Localization\ITranslator $translator)
    {
        $this->netteTranslator = $translator;
    }

    /**
     * Translate string using Nette translator
     * @param string $string
     * @return string
     */
    private function translate($string)
    {
        return ($this->netteTranslator !== null) ? $this->netteTranslator->translate($string) : $string;
    }

    // ------------------------------------------------------------------------------
    // Annotations internals
    // ------------------------------------------------------------------------------

    /**
     * Parse annotation of property
     * @param Nette\Reflection\Property $property
     */
    private function parsePropertyAnnotation(Nette\Reflection\Property $property)
    {
        // Parse column annotation
        $column = $this->parseAnnotationColumn($property);
        // Parse rest of them
        foreach ($this->propertyAnnotations as $name) {
            if ($property->hasAnnotation($name)) {
                $method = "parseAnnotation$name";
                $this->$method($column, $property->getAnnotation($name));
            }
        }
    }

    /**
     * Parse "Column" annotation
     * @param Nette\Reflection\Property $property
     * @return Column
     */
    private function parseAnnotationColumn(Nette\Reflection\Property $property)
    {
        // Fetch annotation data
        $map = $property->getAnnotation("Column");

        // Determine column definition
        $name = $property->getName();
        $type = ucfirst((isset($map['type'])) ? $map['type'] : 'text');

        // Create column
        if ($type == 'Date') {
            $format = isset($map['format']) ? $map['format'] : null;
            $column = $this->addColumnDate($name, $map['label'], $format);
        } elseif ($type == 'DateTime') {
            $format = isset($map['format']) ? $map['format'] : null;
            $column = $this->addColumnDateTime($name, $map['label'], $format);
        } else {
            $method = "addColumn$type";
            $column = $this->$method($name, $map['label']);
        }
        // Check if mapper set
        if (isset($map['mapper'])) {
            $column->setColumn($map['mapper']);
        }
        return $column;
    }

    /**
     * Parse "Render" annotation
     * @param Column $column
     * @param Nette\Utils\ArrayHash $data
     */
    private function parseAnnotationRender(Column $column, Nette\Utils\ArrayHash $data)
    {
        // Set callable
        if (isset($data['callable'])) {
            $callable = [$this->getModel(), $data['callable']];
            $column->setCustomRender($callable);
            $column->setCustomRenderExport($callable);
        }

        // Set export callable
        if (isset($data['export'])) {
            $column->setCustomRenderExport([$this->getModel(), $data['export']]);
        }

        // Set replacement map
        if (isset($data['map'])) {
            $column->setReplacement($this->getModel()->{$data['map']}());
        }
    }

    /**
     * Parse "Sortable" annotation
     * @param Column $column
     * @param mixed $data
     */
    private function parseAnnotationSortable(Column $column, $data)
    {
        if ($data instanceof Nette\Utils\ArrayHash) {
            // Check if sort is disabled
            if (isset($data['disabled'])) {
                $disabled = $data['disabled'];
                if (is_bool($disabled)) {
                    if ($disabled) {
                        return;
                    }
                } else if ($this->getModel()->{$disabled}()) {
                    return;
                }
            }

            // Set default sort
            if (isset($data['default'])) {
                $column->setDefaultSort($data['default']);
            }

        }
        $column->setSortable();
    }

    /**
     * Parse "Filter" annotation
     * @param Column $column
     * @param mixed $data
     */
    private function parseAnnotationFilter(Column $column, $data)
    {
        if ($data instanceof Nette\Utils\ArrayHash) {
            // Check if filter is disabled
            if (isset($data["disabled"])) {

                $disabled = $data['disabled'];
                if (is_bool($disabled)) {
                    if ($disabled) {
                        return;
                    }
                } else if ($this->getModel()->{$disabled}()) {
                    return;
                }
            }

            // Create filter
            $type = ucfirst((isset($data['type'])) ? $data['type'] : 'text');
            switch ($type) {
                case 'Select':
                    $filter = $column->setFilterSelect($this->getModel()->{$data['data']}());
                    break;
                case 'Text':
                    $filter = $column->setFilterText()->setSuggestion();
                    break;
                default:
                    $method = "setFilter$type";
                    $filter = $column->$method();
            }

            // Set default
            if (isset($data['default'])) {
                $value = $data['default'];
                $value = (substr($value, 0, 3) == 'get') ? $this->getModel()->{$value}() : $value;
                $filter->setDefaultValue($value);
            }

            // Set where
            if (isset($data['restrict'])) {
                $filter->setWhere([$this->getModel(), $data['restrict']]);
            } else if (isset($data['column']) && $this->model instanceof NetteDatabase) {
                $col = $data['column'];
                switch ($type) {
                    case 'Text': {
                        $filter->setWhere(function ($value, Selection $selection) use ($col) {
                            $selection->where("$col ILIKE(?)", "%$value%");
                        });
                        break;
                    }
                    case 'Number':
                    case 'Select': {
                        $filter->setWhere(function ($value, Selection $selection) use ($col) {
                            $selection->where($col, $value);
                        });
                        break;
                    }
                    case 'Date': {
                        $filter->setWhere(function ($value, Selection $selection) use ($col) {
                            $date = \DateTime::createFromFormat('d.m.Y', $value);
                            $selection->where("$col::text LIKE ?", $date->format('Y-m-d%'));
                        });
                        break;
                    }
                    default: {
                        $filter->setWhere(function ($value, Selection $selection) use ($col) {
                            $selection->where("$col LIKE ?", $value);
                        });
                    }
                }
            }
        } else {
            $column->setFilterText()->setSuggestion();
        }
    }

    /**
     * Parse "Primary" annotation
     * @param string $data
     */
    private function parseAnnotationPrimary($data)
    {
        $this->setPrimaryKey($data);
    }

    /**
     * Parse "RenderRow" annotation
     * @param Nette\Utils\ArrayHash $data
     */
    private function parseAnnotationRenderRow(Nette\Utils\ArrayHash $data)
    {
        $this->setRowCallback([$this->getModel(), $data['callable']]);
    }

    /**
     * Parse "Action" annotation
     * @param Nette\Utils\ArrayHash $data
     */
    private function parseAnnotationAction(Nette\Utils\ArrayHash $data)
    {
        // Create action
        $action = $this->addActionHref($data['name'], $data['label']);

        // Set icon
        if (isset($data['icon'])) {
            $action->setIcon($data['icon']);
        }

        // Set disabled
        if (isset($data['restrict'])) {
            $action->setDisable([$this->getModel(), $data['restrict']]);
        }

        // Set href
        if (isset($data['href'])) {
            $action->setCustomHref([$this->getModel(), $data['href']]);
        }
    }

    /**
     * Parse "DefaultSort" annotation.
     * @param Nette\Utils\ArrayHash $data
     * @throws \Grido\Exception
     */
    private function parseAnnotationDefaultSort(Nette\Utils\ArrayHash $data)
    {
        $this->setDefaultSort((array)$data);
    }

    // ------------------------------------------------------------------------------
    // Override methods
    // ------------------------------------------------------------------------------

    /** @inheritdoc */
    public function setModel($model, $forceWrapper = FALSE)
    {
        if ($model instanceof Selection) {
            $model = new NetteDatabase($model);
        }
        return parent::setModel($model, $forceWrapper);
    }

    /**
     * @inheritdoc
     * @deprecated
     */
    public function setTranslator(Nette\Localization\ITranslator $translator)
    {
        return parent::setTranslator($translator);
    }

    /** @inheritdoc */
    public function addActionHref($name, $label, $destination = NULL, array $args = [])
    {
        return parent::addActionHref($name, $this->translate($label), $destination, $args);
    }

    /**
     * Create ajax href action.<br>
     * <b>WARNING:</b> Do NOT set custom renderer or "ajax" class will not set.
     * @param string $name
     * @param string $label
     * @param string|null $destination
     * @param array|NULL $args
     * @return \Grido\Components\Actions\Href
     */
    public function addActionHrefAjax($name, $label, $destination = NULL, array $args = [])
    {
        $action = $this->addActionHref($name, $label, $destination, $args);
        $action->setCustomRender(function ($row, $el) {
            $el->addClass('ajax');
            return $el;
        });
        return $action;
    }

    /** @inheritdoc */
    public function addActionEvent($name, $label, $onClick = NULL)
    {
        return parent::addActionEvent($name, $this->translate($label), $onClick);
    }

    /** @inheritdoc */
    public function addColumnDate($name, $label, $dateFormat = NULL)
    {
        if (!isset($dateFormat))
            $dateFormat = $this->dateService->getDateFormat();
        return parent::addColumnDate($name, $this->translate($label), $dateFormat);
    }

    public function addColumnDateTime($name, $label, $dateFormat = NULL)
    {
        if (!isset($dateFormat))
            $dateFormat = $this->dateService->getDateTimeFormat();
        return parent::addColumnDate($name, $this->translate($label), $dateFormat);
    }

    /** @inheritdoc */
    public function addColumnEmail($name, $label)
    {
        return parent::addColumnEmail($name, $this->translate($label));
    }

    /** @inheritdoc */
    public function addColumnLink($name, $label)
    {
        return parent::addColumnLink($name, $this->translate($label));
    }

    /** @inheritdoc */
    public function addColumnNumber($name, $label, $decimals = NULL, $decPoint = NULL, $thousandsSep = NULL)
    {
        return parent::addColumnNumber($name, $this->translate($label), $decimals, $decPoint, $thousandsSep);
    }

    /** @inheritdoc */
    public function addColumnText($name, $label)
    {
        return parent::addColumnText($name, $this->translate($label));
    }


    /**
     * Copied from original sources and edited.
     * @inheritdoc
     */
    protected function applySorting()
    {
        $sort = [];
        $this->sort = $this->sort ? $this->sort : $this->defaultSort;

        foreach ($this->sort as $column => $dir) {
            $component = $this->getColumn($column, FALSE);
            if (!$component) {
                if (!isset($this->defaultSort[$column])) {
                    $this->__triggerUserNotice("Column with name '$column' does not exist.");
                    break;
                }

            }

            if (!in_array($dir, [Column::ORDER_ASC, Column::ORDER_DESC])) {
                if ($dir == '' && isset($this->defaultSort[$column])) {
                    unset($this->sort[$column]);
                    break;
                }

                $this->__triggerUserNotice("Dir '$dir' is not allowed.");
                break;
            }

            $sort[$component ? $component->column : $column] = $dir == Column::ORDER_ASC ? 'ASC' : 'DESC';
        }

        if (!empty($sort)) {
            $this->getModel()->sort($sort);
        }
    }
}

/**
 * Interface IGridFactory
 * @package App\Components\Grido
 */
interface IGridFactory
{
    /** @return Grid */
    function create();
}
