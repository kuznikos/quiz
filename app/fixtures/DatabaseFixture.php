<?php
namespace App\Fixtures;

use Nette\Database\Context;

/**
 * Class DatabaseFixture
 * @package App\Fixtures
 */
class DatabaseFixture
{
    /**
     * Function called on {@link Context} class initialize.
     * @param Context $db
     */
    public function initialize(Context $db)
    {
        try {
            $db->table('user')->fetch();
        } catch (\Exception $e) {
            $this->initStructure($db);
            // Refresh database structure cache.
            // (fixes crash "Table 'user' does not exist." which occurs first time the project is started.)
            $db->getStructure()->rebuild();
            $this->initData($db);
        }
    }

    /**
     * Create database structure.
     * @param Context $db
     */
    private function initStructure(Context $db)
    {
        $db->getConnection()->getPdo()->exec('
            CREATE TABLE "user" (
              "user_id" integer NOT NULL PRIMARY KEY,
              "username" text(8) NOT NULL
            );
            CREATE TABLE "student" (
              "student_id" integer NOT NULL PRIMARY KEY,
              "name" text(30) NOT NULL
            );
            CREATE TABLE "student_score" (
              "student_score_id" integer NOT NULL PRIMARY KEY,
              "student_id" integer NOT NULL,
              "score" real NOT NULL,
              "date" text NOT NULL,
              FOREIGN KEY ("student_id") REFERENCES "student" ("student_id")
            );
            CREATE TABLE "posts" (
              "post_id" integer NOT NULL PRIMARY KEY,
              "name" text(40) NOT NULL,
              "content" text(700) NOT NULL,
              "author" text(30) NOT NULL
            );
            CREATE TABLE "test" (
              "test_id" INTEGER NOT NULL PRIMARY KEY,
              "name" text(250) NOT NULL,
              "datetime" text NOT NULL
            );
            CREATE TABLE "student_to_test" (
              "test_id" INTEGER NOT NULL,
              "student_id" INTEGER NOT NULL,
              FOREIGN KEY ("test_id") REFERENCES "test" ("test_id"),
              FOREIGN KEY ("student_id") REFERENCES "student" ("student_id")
            );
        ');
    }

    /**
     * Insert basic data to database.
     * @param Context $db
     */
    private function initData(Context $db)
    {
        // Insert user
        $db->table('user')->insert(['username' => 'admin']);

        // Task one
        $db->table('student')->insert([
            ['student_id' => 1, 'name' => 'Franta Vomáčka'],
            ['student_id' => 2, 'name' => 'Tonda Javor'],
            ['student_id' => 3, 'name' => 'Božena Němcová'],
            ['student_id' => 4, 'name' => 'Láďa Vagner'],
            ['student_id' => 5, 'name' => 'Olda Bouchač'],
            ['student_id' => 6, 'name' => 'Hermiona Grangerová'],
            ['student_id' => 7, 'name' => 'Ezio Auditore'],
        ]);

        $db->table('student_score')->insert([
            ["student_score_id" => 1, "student_id" => 4, "score" => 9999, "date" => '2016-12-12'],
            ["student_score_id" => 2, "student_id" => 1, "score" => 24.5, "date" => '2017-01-18'],
            ["student_score_id" => 3, "student_id" => 3, "score" => 85, "date" => '2017-02-03'],
            ["student_score_id" => 4, "student_id" => 5, "score" => 49.1, "date" => '2017-02-07'],
            ["student_score_id" => 5, "student_id" => 7, "score" => 10, "date" => '1481-07-18'],
            ["student_score_id" => 6, "student_id" => 6, "score" => 98, "date" => '1999-09-19']
        ]);

        // Task two
        $db->table('posts')->insert([
            ['post_id' => 1, 'name' => 'Příspěvek 1', 'author' => 'admin', 'content' => 'Vero sunt vel aut velit ullam explicabo eveniet. Quas cum et vero praesentium aliquam. Vitae aut at eos nemo quo et aut modi. Placeat error est et consequatur. Natus ea iste ut qui dolores. Non enim ab tempora recusandae tempora delectus. Mollitia ad at enim eaque quibusdam error. Quisquam sunt quidem suscipit. Consectetur quia provident dolores reiciendis qui harum voluptatibus qui. Alias non quae et recusandae nisi et. Occaecati qui quia atque consequatur eum voluptatem porro maxime. Voluptatem minus perspiciatis. Quod non ut aspernatur vitae accusamus consequatur.'],
            ['post_id' => 2, 'name' => 'Příspěvek 2', 'author' => 'administrator', 'content' => 'Et reiciendis enim labore facere culpa libero delectus. Autem eum non. Ab ullam sint voluptatem aut quod et qui fugiat. Incidunt veritatis et doloribus perferendis impedit. Sit expedita voluptas tempore nihil in commodi. Suscipit veniam non vero corrupti nihil. Itaque provident repellendus voluptatibus enim. Dolores ut velit explicabo fugit porro. Ut laborum cupiditate totam adipisci excepturi mollitia qui qui. Aut quod modi debitis dicta veniam alias fuga. Quia dicta cumque qui id et. Perspiciatis magnam ducimus fugit fugiat deserunt nemo. Rerum itaque veritatis ea cupiditate qui accusamus. In nisi esse possimus exercitationem a eos commodi est nihil.'],
            ['post_id' => 3, 'name' => 'Lorem', 'author' => 'novak3', 'content' => 'Optio in veniam voluptas sint voluptas. Quis doloribus est voluptate ea voluptas distinctio est qui. Placeat excepturi sed soluta amet. Aut error ipsam quas. Corrupti facilis tenetur ut. Nemo commodi possimus facilis qui. Sint deserunt consequatur. Ut animi qui non consectetur. Est fugiat dignissimos est architecto. Minus voluptas at velit molestiae delectus aut quis. Optio ut aut quod nostrum cupiditate eos provident et. Velit eius natus corrupti voluptate suscipit blanditiis incidunt. Ratione enim dolor labore earum dolores vero.']
        ]);

        // Task three
        $db->table('test')->insert([
            ['test_id' => 1, 'name' => 'Zápočet', 'datetime' => '2017-6-6 10:00'],
            ['test_id' => 2, 'name' => 'Oprava zápočtu', 'datetime' => '2017-6-7 10:00'],
            ['test_id' => 3, 'name' => 'Předtermín', 'datetime' => '2017-6-6 10:30'],
            ['test_id' => 4, 'name' => 'Řádný termín 1', 'datetime' => '2017-6-10 9:30'],
            ['test_id' => 5, 'name' => 'Řádný termín 2', 'datetime' => '2017-6-10 13:00'],
            ['test_id' => 6, 'name' => 'Řádný termín 3', 'datetime' => '2017-6-15 10:00'],
        ]);

        $db->table('student_to_test')->insert([
            ['test_id' => 1, 'student_id' => 4],
            ['test_id' => 3, 'student_id' => 4],
            ['test_id' => 3, 'student_id' => 6],
            ['test_id' => 4, 'student_id' => 2],
            ['test_id' => 6, 'student_id' => 1],
        ]);
    }
}