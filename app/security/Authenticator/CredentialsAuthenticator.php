<?php
namespace App\Security\Authenticator;

use App\Model\Users;
use Nette\Security\AuthenticationException;
use Nette\Security\IAuthenticator;
use Nette\Security\Identity;
use Nette\Security\User;

/**
 * Class CredentialsAuthenticator
 * @package App\Security\Authenticator
 */
class CredentialsAuthenticator
{
    /** @var User */
    private $user;
    /** @var Users */
    private $usersModel;

    /**
     * CredentialsAuthenticator constructor.
     * @param User $user
     * @param Users $usersModel
     */
    public function __construct(User $user, Users $usersModel)
    {
        $this->user = $user;
        $this->usersModel = $usersModel;
    }

    /**
     * Login user to system.
     * @param array $data
     * @throws AuthenticationException
     */
    public function login(array $data = [])
    {
        $username = $this->fetchUsernameLocal($data);

        // Check if user exist
        $userData = $this->usersModel->getUser($username);
        if ($userData == null) {
            throw new AuthenticationException('base.forms.usernameIncorrect', IAuthenticator::IDENTITY_NOT_FOUND);
        }
        $this->user->setExpiration('2 hours');
        // Login user
        $this->user->login(new Identity($userData['user_id'], NULL, $userData));
    }

    /**
     * Gets username when user tries to login in development mode.
     * @param array $data
     * @return mixed
     * @throws AuthenticationException
     */
    private function fetchUsernameLocal(array $data)
    {
        // Check if username is set.
        if (empty($data[0])) {
            throw new AuthenticationException('base.form.usernameRequired', IAuthenticator::USERNAME);
        }
        return $data[0];
    }
}