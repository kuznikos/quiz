<?php
namespace App\Presenters;
use App\Components\Grido\Grid;
use Ublaboo\DataGrid\DataGrid;

/**
 * Class TaskThreePresenter
 * @package App\Presenters
 */
class TaskThreePresenter extends BasePresenter
{
    public function actionDefault()
    {

    }

    public function renderStudents($id)
    {
        $this->template->test = $this->db->table('test')->where('test_id', $id)->fetch();
    }

    public function actionAddStudent($id, $testId)
    {
        $this->db->table('student_to_test')
            ->insert([
                'student_id' => $id,
                'test_id' => $testId
            ]);
        $this->redirect('students', $testId);
    }

    public function actionRemoveStudent($id, $testId)
    {
        $this->db->table('student_to_test')
            ->where('student_id', $id)
            ->where('test_id', $testId)
            ->delete();
        $this->redirect('students', $testId);
    }

    public function createComponentTestGrid()
    {
        $g = new Grid();
        $new_grid = new DataGrid();

        $tests = $this->db->table('test')->fetchAll();

        $testArray = [];
        foreach($tests as $test)
        {
            $count = $this->db->table('student_to_test')
                ->select('count(*) studentCount')
                ->where('test_id', $test['test_id'])
                ->fetch();
            $testArray[] = [
                'id' => $test['test_id'],
                'name' => $test['name'],
                'datetime' => $test['datetime'],
                'studentCount' => $count['studentCount']
            ];
        }

        $new_grid->setPrimaryKey('id');
        $new_grid->setDataSource($testArray);
        $new_grid->addColumnText('name', 'Název testu')->setSortable();
        $new_grid->addColumnDateTime('datetime', 'Datum konání')->setFormat('d.m.Y h:i')->setSortable();
        $new_grid->addColumnNumber('studentCount', 'studentů')->setSortable();
        $new_grid->addAction('students', 'studenti');


        return $new_grid;
    }

    private function getSignedStudents($testId)
    {
        return $this->db->table('student_to_test')
            ->select('student.student_id id, student.name')
            ->where('test_id', $testId)
            ->fetchAll();
    }

    public function createComponentSignedStudentsGrido()
    {
        $new_grid = new DataGrid();
        $id = $this->getParameter('id');

        $students = $this->getSignedStudents($id);

        $new_grid->setPrimaryKey('id');
        $new_grid->setDataSource($students);
        $new_grid->addColumnText('name', 'jméno');
        $new_grid->addAction('removeStudent', 'odebrat', null, ['id'] )->addParameters(['testId' => $id]);

        return $new_grid;
    }

    public function createComponentUnsignedStudents()
    {
        $new_grid = new DataGrid();
        $id = $this->getParameter('id');
        $signedStudents = [];
        foreach($this->getSignedStudents($id) as $student)
        {
            $signedStudents[] = $student['id'];
        }
        if (sizeof($signedStudents) == 0) {
            $students = $this->db->table('student')
                ->select('student_id id, name')
                ->fetchAll();
        }
        else{
            $students = $this->db->table('student')
                ->select('student_id id, name')
                ->where('id NOT', $signedStudents)
                ->fetchAll();
        }

        $new_grid->setDataSource($students);
        $new_grid->addColumnText('name', 'jméno');
        $new_grid->addAction('addStudent', 'přidat', null, ['id'] )->addParameters(['testId' => $id]);

        return $new_grid;
    }
}
