<?php
namespace App\Presenters;

use App\Components\TaskTwo\PostForm\IPostFormFactory;
use App\Model\Post\Post;
use App\Model\Post\PostRepository;

/**
 * Class TaskTwoPresenter
 * @package App\Presenters
 */
class TaskTwoPresenter extends BasePresenter
{
    /** @var IPostFormFactory @inject */
    public $postFormFactory;
    /** @var PostRepository @inject */
    public $postsRepository;

    /** @var Post|null */
    private $post;

    public function actionNewPost()
    {
        // Do nothingabcder
    }

    public function actionEditPost($postId)
    {
        $this->post = $this->postsRepository->fetchById($postId);
        if ($this->post === null) {
            $this->flashMessage('Nelze upravit neexistující příspěvek.', 'error');
            $this->redirect('default');
        }
    }

    public function actionShowPost($postId)
    {
        $this->post = $this->postsRepository->fetchById($postId);
        if ($this->post === null) {
            $this->flashMessage('Nelze zobrazit neexistující příspěvek.', 'error');
            $this->redirect('default');
        }
    }

    public function handleDeletePost($postId)
    {
        $this->postsRepository->remove($this->postsRepository->fetchById($postId));
    }

    public function renderDefault()
    {
        $this->template->posts = $this->postsRepository->fetchAll();
    }

    public function renderShowPost()
    {
        $this->template->post = $this->post;
    }

    /**
     * @return \App\Components\TaskTwo\PostForm\PostForm
     */
    public function createComponentPostForm()
    {
        $form = $this->postFormFactory->create($this->post);
        $form->onReturn[] = function () {
            $this->redirect('default');
        };
        $form->onSend[] = function () {
            $this->flashMessage('Příspěvek byl uložen.', 'success');
            $this->redirect('default');
        };
        return $form;
    }
}
