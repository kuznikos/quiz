<?php
namespace App\Presenters;

use App\Forms\SignUserFormFactory;
use App\Security\Authenticator\CredentialsAuthenticator;
use Nette\Application\UI\Form;

class SignPresenter extends BasePresenter
{
    /** @var SignUserFormFactory @inject */
    public $factory;
    /** @var CredentialsAuthenticator @inject */
    public $authenticator;

    public function startup()
    {
        $this->setRequireLogin(false);
        parent::startup();
    }

    public function actionOut()
    {
        $this->getUser()->logout();
        if (!$this->user->isLoggedIn()) {
            $this->flashMessage('base.loggedOutMsg', 'success');
        } else {
            $this->flashMessage('base.loggedOutError', 'error');
        }
        $this->getSession()->close();
        $this->redirect("Homepage:");
    }

    public function actionIn()
    {

    }

    /**
     * Sign-in user form factory.
     * @return Form
     */
    protected function createComponentSignInUserForm()
    {
        $form = $this->factory->create();
        $form->onSuccess[] = function () {
            $this->redirect("Homepage:default");
        };
        return $form;
    }
}
