<?php
namespace App\Presenters;

/**
 * Class HomepagePresenter
 * @package App\Presenters
 */
class HomepagePresenter extends BasePresenter
{
    /**
     * Disable login requirement for this presenter.
     */
    public function startup()
    {
        $this->setRequireLogin(false);
        parent::startup();
    }
}
