<?php
namespace App\Presenters;

/**
 * Class TaskOnePresenter
 * @package App\Presenters
 */
class TaskOnePresenter extends BasePresenter
{
    public function actionDefault()
    {
        $this->template->students = $this->db->table("student_score")->fetchAll();
    }
}