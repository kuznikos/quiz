<?php
namespace App\Presenters;

use Nette;

abstract class BasePresenter extends Nette\Application\UI\Presenter
{
    /** @var \Kdyby\Translation\Translator @inject */
    public $translator;
    /** @var Nette\Database\Context @inject */
    public $db;
    /** @var \App\Components\Menu\Sidebar\IMenuControlFactory @inject */
    public $sidebarMenuFactory;
    /** @var \App\Components\Menu\Header\IMenuControlFactory @inject */
    public $headerMenuFactory;

    /** @var bool */
    protected $requireLogin = true;

    public function startup()
    {
        parent::startup();
        $this->template->withoutSidebar = false;
        $this->template->moduleName = $this->getModuleName();
        if ($this->requireLogin) {
            $this->requireLoggedUser();
        }
    }

    public function getModuleName()
    {
        if (strpos($this->name, ':')) {
            return explode(':', $this->name)[0];
        } else {
            return '';
        }
    }

    protected function requireLoggedUser()
    {
        if (!$this->user->loggedIn) {
            $this->flashMessage('base.requireLoggedUser', 'error');
            $this->redirect(':Homepage:default');
        }
    }

    protected function setRequireLogin($bool)
    {
        $this->requireLogin = $bool;
    }

    /**
     * @return \App\Components\Menu\Sidebar\MenuControl
     */
    protected function createComponentSidebarMenu()
    {
        return $this->sidebarMenuFactory->create();
    }

    /**
     * @return \App\Components\Menu\Header\MenuControl
     */
    protected function createComponentHeaderMenu()
    {
        return $this->headerMenuFactory->create();
    }

    /**
     * Basically just helper for IDE because of return annotation
     *
     * @return \DK\Menu\Menu
     */
    protected function getMenu()
    {
        return $this['sidebarMenu']->getMenu();
    }
}
