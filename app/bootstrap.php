<?php
require __DIR__ . '/../vendor/autoload.php';

$configurator = new Nette\Configurator;

// Detect environment
// See {@link Environment} for allowed values.
$appMode = getenv('APP_ENV') ?: ($configurator::detectDebugMode() ? 'local' : 'production');
$enableDebug = (($appMode == 'local') || (isset($_COOKIE['debugMode']) && $_COOKIE['debugMode'] == 'true'));

// Initialize configurator
$configurator->setTimeZone('Europe/Prague');
$configurator->setDebugMode($enableDebug);
$configurator->enableDebugger(__DIR__ . '/../log');
$configurator->setTempDirectory(__DIR__ . '/../temp');
$configurator->addParameters([
    'appMode' => $appMode
]);
$configurator->createRobotLoader()
    ->addDirectory(__DIR__)
    ->register();

// Load config files
$configurator->addConfig(__DIR__ . '/config/config.neon');

return $configurator->createContainer();
