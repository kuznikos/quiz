# DBS QUIZ

* Tento kvíz slouží jako úvod do předmětu BI-SP1::DBSproject
* Jedná se o malou aplikaci v Nette, která obsahuje 3 úkoly


* Problémy s nasazením a další dotazy pokládejte na [Slacku v kanálu quiz](https://dbsfitcvutcz.slack.com/messages/quiz/)

### Co potřebuje pro spuštění

* Apache
* PHP 5.6 (PHP 7+ není podporováno)
* volitelně: git (stejně bude potřeba pro práci na newDBS projektu)

---
* databáze (SQLite) se vytvoří sama při prvním použití
* _Pokud nejste na Unixovém stroji, je možné využít XAMPP či jiný balík webserveru_
    * _V nich je často potřeba povolit rozšíření pro SQLite3_ 


### Jak na to

* Zkopírujte si tento repozitář
    * Buď si vytvořte fork do svého namespace
    * Nebo stáhněte jako [ZIP](https://gitlab.fit.cvut.cz/malecold/newDBS-quiz/repository/archive.zip?ref=master) a poté nahrajte do vlastního repozitáře zde na Gitlabu
* Svůj repozitář si poté naklonujte lokálně.
    * `git clone git@gitlab.fit.cvut.cz:váš_username/newDBS-quiz.git`
    * Pokud neumíte s gitem, určitě se ho [naučíte](http://rogerdudler.github.io/git-guide/) během chvilky
* Dále je dobré nastavit group na webserver (typicky `www-data` nebo `apache2`)
    
        cd newDBS-quiz
        chgrp -R www-data www/
        chgrp -R www-data temp/
        chgrp -R www-data log/
      
* Databázi není třeba konfigurovat, vytvoří se sama lokálně. Pokud s ní máte problémy, vizte [Troubleshooting](#troubleshooting) nebo se zeptejte na Slacku, viz úvod

* Nakonfigurujte apache. Stačí například
  
        <VirtualHost *:80>
            ServerName localhost
            DocumentRoot /var/www/quizDBS/www/
            <Directory /var/www/quizDBS/www>
                Options FollowSymLinks
                AllowOverride All
            </Directory>
        </VirtualHost>
        
* Aplikace by nyní měla běžet na vašem lokálním stroji 

### Vaše řešení

* Ideálně formou forknutého repozitáře zde na gitlabu
* Pokud máte repozitář privátní, udělte přístup uživatelům Oldřich Malec, Pavel Kovář a Marek Erben


### Troubleshooting

* Nelze se přihlásit (v databázi není user admin) nebo jiný problém s databází
    * Může nastat například při NFS sdílení souborů (vagrant nad Windows)
    * __Řešení__:
        * změňte v `app/config/config.neon` umístění databázového souboru do místa nesdíleného přes NFS a zkontrolujte, že má apache právo pro zápis do nově zvolené složky
        * nastavte databazovému souboru `database.sql` vyšší práva (pro účely kvízu klidně 777)
* Nelze se připojit k databázi (Nette\Database\ConnectionException - could not find driver)
    * Chyba může spočívat ve špatné verzi SQLite
    * Správná verze se dá stáhnout z adresy [https://packages.debian.org/buster/php-sqlite3](https://packages.debian.org/buster/php-sqlite3)
* Stránka přihlašování neexistuje
    * Zkontrolujte, zda má apache zapnutý `mod_rewrite`. Pokud ne, je možné jej zapnout:
    
            sudo a2enmod rewrite
            sudo systemctl restart apache2
    
